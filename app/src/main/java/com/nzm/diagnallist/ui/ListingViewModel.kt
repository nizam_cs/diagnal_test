package com.nzm.diagnallist.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import com.nzm.diagnallist.data.model.Content
import com.nzm.diagnallist.data.repository.MoviesRepository
import com.nzm.diagnallist.ui.adapter.ItemDataSourceFactory

class ListingViewModel(private val moviesRepository: MoviesRepository): ViewModel() {

    private var itemDataSourceFactory: ItemDataSourceFactory
    var contentLiveData: LiveData<PagedList<Content>>
    var pageTitle: MutableLiveData<String> = MutableLiveData()
    var filterText = ""

    init {
        val config: PagedList.Config = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(true)
            .build()
        itemDataSourceFactory = ItemDataSourceFactory(viewModelScope, moviesRepository)
        contentLiveData = LivePagedListBuilder<Int, Content>(itemDataSourceFactory, config).build()
        itemDataSourceFactory.itemLiveDataSource.observeForever {
            it?.pageTitle?.observeForever { title ->
                if (!title.isNullOrEmpty()) pageTitle.value = title
            }
        }
    }

    fun filterList(query: String) {
        if (filterText != query) {
            filterText = query
            itemDataSourceFactory.filter(query)
            contentLiveData.value?.dataSource?.invalidate()
        }
    }


}