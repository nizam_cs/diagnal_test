package com.nzm.diagnallist.ui.adapter

import android.content.ClipData.Item
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.nzm.diagnallist.data.model.Content
import com.nzm.diagnallist.data.repository.MoviesRepository
import kotlinx.coroutines.CoroutineScope


class ItemDataSourceFactory(
    private val scope: CoroutineScope,
    private val moviesRepository: MoviesRepository
): DataSource.Factory<Int, Content>() {

    var filterQuery = ""
    val itemLiveDataSource =
        MutableLiveData<ItemDataSource>()

    override fun create(): DataSource<Int, Content> {
        Log.d("Nzm","create data source with filter: $filterQuery")
        val itemDataSource = ItemDataSource(scope, moviesRepository, filterQuery)
        itemLiveDataSource.postValue(itemDataSource)
        return itemDataSource
    }

    fun filter(query: String) {
        filterQuery = query
    }
}