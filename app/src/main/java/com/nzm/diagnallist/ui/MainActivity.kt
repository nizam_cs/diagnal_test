package com.nzm.diagnallist.ui

import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nzm.diagnallist.R
import com.nzm.diagnallist.databinding.ActivityMainBinding
import com.nzm.diagnallist.ui.adapter.MoviesAdapter
import com.nzm.diagnallist.ui.adapter.SpacesItemDecoration
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MoviesAdapter
    lateinit var binding: ActivityMainBinding
    private val mainViewModel: ListingViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewmodel = mainViewModel
        binding.lifecycleOwner = this
        setSupportActionBar(binding.toolBar)
        title = ""
        binding.back.setOnClickListener { onBackPressed() }

        binding.list.layoutManager =
            GridLayoutManager(this, resources.getInteger(R.integer.grid_span_count))
        binding.list.addItemDecoration(
            SpacesItemDecoration(
                resources.getDimensionPixelSize(R.dimen.card_margin_start),
                resources.getDimensionPixelSize(R.dimen.card_margin_bottom)
            )
        )
        binding.list.setHasFixedSize(true)

        adapter = MoviesAdapter(this)
        mainViewModel.contentLiveData.observe(this, Observer {
            adapter.submitList(it)
        })
        binding.list.adapter = adapter

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchView = menu!!.findItem(R.id.action_search)
            .actionView as SearchView
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    when {
                        query.length >= 3 -> {
                            mainViewModel.filterList(query)
                            adapter.filterText = query
                        }
                        query.isEmpty() -> {
                            mainViewModel.filterList(query)
                        }
                        else -> {
                            val snack = Snackbar.make(
                                binding.list,
                                "Please enter atleat 3 chars to search",
                                Snackbar.LENGTH_SHORT
                            )
                            val view: View = snack.view
                            val params =
                                view.layoutParams as FrameLayout.LayoutParams
                            params.gravity = Gravity.TOP
                            params.topMargin = binding.toolBar.height
                            view.layoutParams = params
                            snack.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
                            snack.show()
                        }
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText.length >= 3 || newText.isEmpty()) {
                        mainViewModel.filterList(newText)
                        adapter.filterText = newText

                    }
                }
                return false
            }
        })

        return true
    }
}