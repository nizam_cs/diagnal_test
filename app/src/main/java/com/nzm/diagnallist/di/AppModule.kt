package com.nzm.diagnallist.di

import com.nzm.diagnallist.data.repository.MoviesRepository
import com.nzm.diagnallist.data.repository.MoviesRepositoryImpl
import com.nzm.diagnallist.ui.ListingViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    //single instance of MoviesRepository
    single<MoviesRepository> { MoviesRepositoryImpl(get()) }

    //ViewModel
    viewModel { ListingViewModel(get()) }
}