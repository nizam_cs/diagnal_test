package com.nzm.diagnallist

import android.app.Application
import com.nzm.diagnallist.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class DiagnalApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@DiagnalApplication)
            modules(appModule)
        }
    }
}