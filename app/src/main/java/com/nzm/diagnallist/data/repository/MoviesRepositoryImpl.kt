package com.nzm.diagnallist.data.repository

import android.content.Context
import com.google.gson.Gson
import com.nzm.diagnallist.data.model.PageResponseModel
import com.nzm.diagnallist.utils.AssetUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MoviesRepositoryImpl(private val context: Context) : MoviesRepository {

    override suspend fun getPageContent(pageNumber: Int): PageResponseModel? {
        return withContext(Dispatchers.IO) {
            val jsonString = AssetUtil.getJsonFromAssets(context, getFileName(pageNumber))
            Gson().fromJson(jsonString, PageResponseModel::class.java)
        }
    }

    private fun getFileName(pageNumber: Int): String {
        return when (pageNumber) {
            1 -> "page1.json"
            2 -> "page2.json"
            3 -> "page3.json"
            else -> "page1.json"
        }
    }
}