package com.nzm.diagnallist.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PageResponseModel : Serializable {
    @SerializedName("page")
    @Expose
    var page: Page? = null

    companion object {
        private const val serialVersionUID = -5961197658517356001L
    }
}