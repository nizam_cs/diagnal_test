package com.nzm.diagnallist.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Content : Serializable {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("poster-image")
    @Expose
    var posterImage: String? = null

    companion object {
        private const val serialVersionUID = 1269381565754933792L
    }
}