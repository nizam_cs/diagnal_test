package com.nzm.diagnallist.data.repository

import android.content.Context
import com.nzm.diagnallist.data.model.PageResponseModel

interface MoviesRepository {
    suspend fun getPageContent(pageNumber: Int): PageResponseModel?
}