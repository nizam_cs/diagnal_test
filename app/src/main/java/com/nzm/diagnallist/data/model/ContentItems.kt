package com.nzm.diagnallist.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ContentItems : Serializable {
    @SerializedName("content")
    @Expose
    var content: List<Content>? = null

    companion object {
        private const val serialVersionUID = 4013623203824557101L
    }
}